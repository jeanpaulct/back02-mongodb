package com.practitioner.back02.service.impl;

import java.util.List;
import java.util.Optional;

import com.practitioner.back02.dto.AccountDto;
import com.practitioner.back02.dto.CustomerDto;
import com.practitioner.back02.mapper.AccountMapper;
import com.practitioner.back02.mapper.CustomerMapper;
import com.practitioner.back02.repository.AccountRepository;
import com.practitioner.back02.repository.CustomerRepository;
import com.practitioner.back02.repository.model.AccountModel;
import com.practitioner.back02.repository.model.CustomerModel;
import com.practitioner.back02.service.CustomerService;
import com.practitioner.back02.service.exception.CustomerAlreadyExistsException;
import com.practitioner.back02.service.exception.CustomerNotFoundException;

import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

	private final CustomerRepository customerRepository;
	private final CustomerMapper customerMapper;
	private final AccountRepository accountRepository;
	private final AccountMapper accountMapper;

	public CustomerServiceImpl(CustomerRepository customerRepository, CustomerMapper customerMapper, AccountRepository accountRepository, AccountMapper accountMapper) {
		this.customerRepository = customerRepository;
		this.customerMapper = customerMapper;
		this.accountRepository = accountRepository;
		this.accountMapper = accountMapper;
	}

	@Override
	public List<CustomerDto> getAll() {
		return customerMapper.mapDto(customerRepository.findAll());
	}

	private CustomerModel getByIdModel(String customerNumber) {
		Optional<CustomerModel> m = customerRepository.findById(customerNumber);
		if (m.isEmpty())
			throw new CustomerNotFoundException(customerNumber);
		return m.get();
	}

	@Override
	public CustomerDto getById(String customerNumber) {
		return customerMapper.mapDto(this.getByIdModel(customerNumber));
	}

	@Override
	public CustomerDto create(CustomerDto customerDto) {
		if (customerRepository.findById(customerDto.getCustomerNumber()).isPresent())
			throw new CustomerAlreadyExistsException(customerDto.getCustomerNumber());
		CustomerModel m = customerMapper.mapModel(customerDto);
		return customerMapper.mapDto(customerRepository.insert(m));
	}

	@Override
	public CustomerDto update(CustomerDto customerDto) {
		if (customerRepository.findById(customerDto.getCustomerNumber()).isEmpty())
			throw new CustomerNotFoundException(customerDto.getCustomerNumber());
		CustomerModel m = customerMapper.mapModel(customerDto);
		return customerMapper.mapDto(customerRepository.save(m));
	}

	@Override
	public CustomerDto updatePartial(CustomerDto customerDto) {
		if (customerRepository.findById(customerDto.getCustomerNumber()).isEmpty())
			throw new CustomerNotFoundException(customerDto.getCustomerNumber());
		CustomerModel m = customerMapper.mapModel(customerDto);
		customerRepository.partialContent(m);
		return customerDto;
	}

	@Override
	public void remove(String customerNumber) {
		customerRepository.deleteById(customerNumber);
	}

	@Override
	public AccountDto addAccountCustomer(String document, AccountDto accountDto) {
		CustomerModel customerModel = this.getByIdModel(document);
		accountDto.setCustomerDocument(customerModel.getCustomerNumber());
		accountDto.setAvailableBalance(0d);
		AccountModel model = this.accountRepository.save(accountMapper.map(accountDto));
		this.customerRepository.accountInCustomer(customerModel, model);
		return accountMapper.map(model);
	}
}
