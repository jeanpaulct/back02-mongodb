package com.practitioner.back02.service;

import java.util.List;

import com.practitioner.back02.dto.AccountDto;

public interface AccountService {
	List<AccountDto> getAccountsByCustomer(String customer);
}
