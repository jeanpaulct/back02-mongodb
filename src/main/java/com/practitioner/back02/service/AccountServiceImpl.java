package com.practitioner.back02.service;

import java.util.List;

import com.practitioner.back02.dto.AccountDto;
import com.practitioner.back02.mapper.AccountMapper;
import com.practitioner.back02.repository.AccountRepository;
import com.practitioner.back02.repository.model.AccountModel;

import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

	private final AccountRepository accountRepository;
	private final AccountMapper accountMapper;

	public AccountServiceImpl(AccountRepository accountRepository, AccountMapper accountMapper) {
		this.accountRepository = accountRepository;
		this.accountMapper = accountMapper;
	}

	@Override
	public List<AccountDto> getAccountsByCustomer(String customer) {
		List<AccountModel> lst = this.accountRepository.findAllByCustomerDocument(customer);
		return accountMapper.map(lst);
	}
}
