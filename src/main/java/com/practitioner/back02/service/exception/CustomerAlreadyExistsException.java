package com.practitioner.back02.service.exception;

public class CustomerAlreadyExistsException extends RuntimeException {
	private String document;

	public CustomerAlreadyExistsException(String document) {
		this.document = document;
	}

	public CustomerAlreadyExistsException(String message, String document) {
		super(message);
		this.document = document;
	}

	public CustomerAlreadyExistsException(String message, Throwable cause, String document) {
		super(message, cause);
		this.document = document;
	}

	public CustomerAlreadyExistsException(Throwable cause, String document) {
		super(cause);
		this.document = document;
	}

	public CustomerAlreadyExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String document) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.document = document;
	}
}
