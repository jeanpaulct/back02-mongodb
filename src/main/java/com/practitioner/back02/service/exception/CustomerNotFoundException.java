package com.practitioner.back02.service.exception;

public class CustomerNotFoundException extends RuntimeException {
	private String document;

	public CustomerNotFoundException(String document) {
		this.document = document;
	}

	public CustomerNotFoundException(String message, String document) {
		super(message);
		this.document = document;
	}

	public CustomerNotFoundException(String message, Throwable cause, String document) {
		super(message, cause);
		this.document = document;
	}

	public CustomerNotFoundException(Throwable cause, String document) {
		super(cause);
		this.document = document;
	}

	public CustomerNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String document) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.document = document;
	}
}
