package com.practitioner.back02.service;

import java.util.List;

import com.practitioner.back02.dto.AccountDto;
import com.practitioner.back02.dto.CustomerDto;

public interface CustomerService {
	List<CustomerDto> getAll();

	CustomerDto getById(String customerNumber);

	CustomerDto create(CustomerDto customerDto);

	CustomerDto update(CustomerDto customerDto);

	CustomerDto updatePartial(CustomerDto customerDto);

	void remove(String customerNumber);

	AccountDto addAccountCustomer(String document, AccountDto accountDto);
}
