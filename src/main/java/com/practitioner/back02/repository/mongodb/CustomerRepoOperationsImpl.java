package com.practitioner.back02.repository.mongodb;

import java.util.ArrayList;

import com.practitioner.back02.repository.model.AccountModel;
import com.practitioner.back02.repository.model.CustomerModel;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
public class CustomerRepoOperationsImpl implements CustomerRepoOperations {

	private final MongoOperations mongoOperations;

	public CustomerRepoOperationsImpl(MongoOperations mongoOperations) {
		this.mongoOperations = mongoOperations;
	}

	private boolean allowReplace(Object newValue) {
		return newValue != null;
	}

	private void setNewValue(Update update, String prop, Object value) {
		if (allowReplace(value))
			update.set(prop, value);
	}

	@Override
	public void partialContent(CustomerModel customerModel) {
		final Query query = query(where("customerNumber").is(customerModel.getCustomerNumber()));
		final Update update = new Update();
		setNewValue(update, "address", customerModel.getAddress());
		setNewValue(update, "phoneNumber", customerModel.getPhoneNumber());
		setNewValue(update, "age", customerModel.getAge());
		setNewValue(update, "fullName", customerModel.getFullName());
		setNewValue(update, "birthDate", customerModel.getBirthDate());
		setNewValue(update, "email", customerModel.getEmail());
		mongoOperations.updateFirst(query, update, CustomerModel.class);
	}


	@Override
	public void accountInCustomer(CustomerModel customer, AccountModel account) {
		if (customer.getAccounts() == null)
			customer.setAccounts(new ArrayList<>());
		customer.getAccounts().add(account.getNumberAccount());
		final Query query = query(where("customerNumber").is(customer.getCustomerNumber()));
		final Update update = new Update();
		update.set("accounts", customer.getAccounts());
		mongoOperations.updateFirst(query, update, CustomerModel.class);
	}
}
