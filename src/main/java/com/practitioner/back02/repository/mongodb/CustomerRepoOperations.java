package com.practitioner.back02.repository.mongodb;

import com.practitioner.back02.repository.model.AccountModel;
import com.practitioner.back02.repository.model.CustomerModel;

public interface CustomerRepoOperations {
	void partialContent(CustomerModel customerModel);

	void accountInCustomer(CustomerModel customerModel, AccountModel account);
}
