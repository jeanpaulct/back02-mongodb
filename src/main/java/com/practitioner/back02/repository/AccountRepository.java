package com.practitioner.back02.repository;

import java.util.List;

import com.practitioner.back02.repository.model.AccountModel;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccountRepository extends MongoRepository<AccountModel, String> {
	List<AccountModel> findAllByCustomerDocument(String customer);
}
