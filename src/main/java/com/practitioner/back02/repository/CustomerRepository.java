package com.practitioner.back02.repository;

import com.practitioner.back02.repository.model.CustomerModel;
import com.practitioner.back02.repository.mongodb.CustomerRepoOperations;

import org.springframework.data.mongodb.repository.MongoRepository;


public interface CustomerRepository extends MongoRepository<CustomerModel, String>, CustomerRepoOperations {
}
