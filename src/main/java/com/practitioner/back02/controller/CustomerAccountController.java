package com.practitioner.back02.controller;

import java.util.List;

import com.practitioner.back02.dto.AccountDto;
import com.practitioner.back02.service.AccountService;
import com.practitioner.back02.service.CustomerService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.practitioner.back02.configuration.ApiConstants.API_CUSTOMER_ACCOUNT;

@RestController
@RequestMapping(API_CUSTOMER_ACCOUNT)
public class CustomerAccountController {

	private final CustomerService customerService;
	private final AccountService accountService;

	public CustomerAccountController(CustomerService customerService, AccountService accountService) {
		this.customerService = customerService;
		this.accountService = accountService;
	}

	@PostMapping
	public AccountDto addAccount(@PathVariable String customerNumber, @RequestBody AccountDto accountDto) {
		return this.customerService.addAccountCustomer(customerNumber, accountDto);
	}

	@GetMapping
	public List<AccountDto> getAccount(@PathVariable String customerNumber) {
		return this.accountService.getAccountsByCustomer(customerNumber);
	}
}
