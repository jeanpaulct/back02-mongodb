package com.practitioner.back02.controller;

import java.util.List;

import com.practitioner.back02.dto.CustomerDto;
import com.practitioner.back02.service.CustomerService;
import com.practitioner.back02.service.exception.CustomerAlreadyExistsException;
import com.practitioner.back02.service.exception.CustomerNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import static com.practitioner.back02.configuration.ApiConstants.API_CUSTOMER;
import static com.practitioner.back02.configuration.ApiConstants._CUSTOMER_PATH;

@RestController
@RequestMapping(API_CUSTOMER)
public class CustomerController {

	private final CustomerService customerService;

	public CustomerController(CustomerService customerService) {
		this.customerService = customerService;
	}

	@GetMapping
	public List<CustomerDto> getAll() {
		return customerService.getAll();
	}

	@GetMapping(_CUSTOMER_PATH)
	public CustomerDto get(@PathVariable String customerNumber) {
		try {
			return customerService.getById(customerNumber);
		}
		catch (CustomerNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping
	public void create(@RequestBody CustomerDto customerDto) {
		try {
			customerService.create(customerDto);
		}
		catch (CustomerAlreadyExistsException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "The customer already exists, check the document number");
		}
	}

	@PutMapping(_CUSTOMER_PATH)
	public void update(@PathVariable String customerNumber, @RequestBody CustomerDto customerDto) {
		customerDto.setCustomerNumber(customerNumber);
		try {
			customerService.update(customerDto);
		}
		catch (CustomerNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@PatchMapping(_CUSTOMER_PATH)
	public void updatePartial(@PathVariable String customerNumber, @RequestBody CustomerDto customerDto) {
		customerDto.setCustomerNumber(customerNumber);
		try {
			customerService.updatePartial(customerDto);
		}
		catch (CustomerNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping(_CUSTOMER_PATH)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remove(@PathVariable String document) {
		customerService.remove(document);
	}

}
