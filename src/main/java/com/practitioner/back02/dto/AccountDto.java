package com.practitioner.back02.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class AccountDto {
	private String numberAccount;
	private String product;
	private Double availableBalance;
	@JsonIgnore
	private String customerDocument;

	public String getNumberAccount() {
		return numberAccount;
	}

	public void setNumberAccount(String numberAccount) {
		this.numberAccount = numberAccount;
	}

	public Double getAvailableBalance() {
		return availableBalance;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public void setAvailableBalance(Double availableBalance) {
		this.availableBalance = availableBalance;
	}

	public String getCustomerDocument() {
		return customerDocument;
	}

	public void setCustomerDocument(String customerDocument) {
		this.customerDocument = customerDocument;
	}
}
