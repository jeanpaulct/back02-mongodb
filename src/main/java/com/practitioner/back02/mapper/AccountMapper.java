package com.practitioner.back02.mapper;

import java.util.List;

import com.practitioner.back02.dto.AccountDto;
import com.practitioner.back02.repository.model.AccountModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountMapper {
	AccountModel map(AccountDto d);

	AccountDto map(AccountModel d);

	List<AccountDto> map(List<AccountModel> lst);
}
